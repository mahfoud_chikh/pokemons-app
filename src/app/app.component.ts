import {Component, OnInit} from '@angular/core';
import {Pokemon} from "./pokemon";
import {LIST_POKEMONS} from "./shared/mock-pokemons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Application des Pokémons';
  pokemons: Pokemon[];

  ngOnInit(): void {
    this.pokemons = LIST_POKEMONS;
  }

}
